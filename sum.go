package main

import (
	"fmt"
)

func main() {
	var num1, num2 int

	fmt.Println("Введите 1 число:")
	fmt.Scanln(&num1)

	fmt.Println("Введите второе число:")
	fmt.Scanln(&num2)

	sum := num1 + num2
	fmt.Println("Сумма чисел:", sum)
}
